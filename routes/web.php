<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function () {
    return redirect('admin');
});
Auth::routes();
Route::get('/order', 'HomeController@index')->name('order');
Route::get('/orders', 'HomeController@getOrders')->name('orders.getorders');
Route::get('/orders/{id}', 'OrderController@getOrders')->name('orders');
Route::get('/order-details/{id}','HomeController@order_details')->name('orders.details');
Route::get('/orders/order-details/{id}','HomeController@order_details')->name('orders.details');
Route::get('/my-orders','OrderController@getMyOrders');
Route::get('/my-orders/{id}','OrderController@getMyOrdersPage');
Route::get('/my-customers','HomeController@getMyCustomrs');
Route::get('/create-order',['uses' => 'OrderController@createOrder'])->name('create.order');
Route::post('/delete-order/{id}','OrderController@deleteOrder');
Route::post('/get-order/{id}','OrderController@getOrder');
Route::get('/customer-order/{id}','OrderController@getOrderByCustomerId')->name('customer.order');
Route::post('/edit-order','OrderController@updateOrder');
Route::post('/createinvoice/{id}','InvoiceController@createInvoice');
Route::get('/invoices','InvoiceController@getInvoices');
Route::get('/downloadinvoice/{id}','InvoiceController@downloadInvoice')->name('downloadinvoice');
Route::get('/invoice/{id}','InvoiceController@invoiceDetails')->name('invoicedetails');
Route::get('/activitylog/','OrderController@getOrderLog');
Route::get('/all-orders-csv/{csvfrom?}/{csvto?}','OrderController@getOrderCsv')->name('download.csv');
