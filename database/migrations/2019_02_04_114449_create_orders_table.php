<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('product_name');
            $table->string('product_link');
            $table->integer('product_qty');
            $table->string('product_comment')->nullable();
            $table->unsignedInteger('sale_by');
            $table->unsignedInteger('purchased_by')->nullable();
            $table->unsignedInteger('status_id');
            $table->integer('bkash_l4digit')->nullable();
            $table->decimal('sale_price_in_dollar',8, 2);
            //$table->decimal('buying_price_in_dollar',8, 2);
            $table->decimal('actual_buying_price',8, 2)->nullable();
            $table->decimal('profit',8, 2);
            $table->decimal('total_order_amount',8, 2);
            $table->decimal('bkash_receive',8, 2)->nullable();
            $table->decimal('bkash_charge',8, 2)->nullable();
            $table->string('advance_adjusted')->nullable();
            $table->date('advanceadjusteddate')->nullable();
            $table->string('card_last_4digit')->nullable();
            $table->decimal('cash_receive',8, 2)->nullable();
            $table->date('cashreceivedate')->nullable();
            $table->decimal('for_courier_charge',8, 2)->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sale_by')->references('id')->on('users');
            $table->foreign('purchased_by')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('status_id')->references('id')->on('order_statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
