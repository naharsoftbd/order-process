<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('subTotal',8,2)->nullable();
            $table->decimal('bkashCharge',8,2)->nullable();
            $table->decimal('deliveryCharge',8,2)->nullable();
            $table->decimal('discount',8,2)->nullable();
            $table->decimal('advancePaid',8,2)->nullable();
            //$table->longText('order_info')->nullable();
            $table->unsignedInteger('agent_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('customer_id');
            $table->foreign('agent_id')
              ->references('id')->on('agents')
              ->onDelete('cascade');
            $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('cascade');
            $table->foreign('customer_id')
              ->references('id')->on('customers')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
