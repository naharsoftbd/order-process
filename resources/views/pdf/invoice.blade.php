<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ $invoice->name }}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
            h1,h2,h3,h4,p,span,div { font-family: DejaVu Sans; }
        </style>
    </head>
    <body>
        <div style="clear:both; position:relative;">
            <div style="position:absolute; left:0pt; width:250pt;">
                <img class="img-rounded" height="auto" src="{{ $invoice->agent->agentLogo }}">
            </div>
            <div style="margin-left:300pt;">
                <b>Date: </b> {{ $invoice->created_at }}<br />
                @if ($invoice->id)
                    <b>Invoice #: </b> {{ $invoice->id }}
                @endif
                <br />
            </div>
        </div>
        <br />
        <h2>{{ $invoice->name }} {{ $invoice->id ? '#' . $invoice->id : '' }}</h2>
        <div style="clear:both; position:relative;">
            <div style="position:absolute; left:0pt; width:250pt;">
                <h4>Business Details:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                     {{$invoice->agent->name}}  <br />
                     ID: {{ $invoice->id }}<br /> 
                     {{$invoice->agent->mobileNo}}  <br />
                     {{$invoice->agent->email}}  <br />
                     {{$invoice->agent->agentAddress}}  <br />
                    </div>
                </div>
            </div>
            <div style="margin-left: 300pt;">
                <h4>Customer Details:</h4>
                <div class="panel panel-default">
                    <div class="panel-body">
                       {{$invoice->customer->customer_name}}  <br />
                     ID: {{ $invoice->id }}<br /> 
                     {{$invoice->customer->mobile}}  <br />
                     {{$invoice->customer->email}}  <br />
                     {{$invoice->customer->address}}  <br />
                     {{$invoice->customer->zipcode}}  <br />
                     {{$invoice->customer->state}}  <br />
                    </div>
                </div>
            </div>
        </div>
        <h4>Items:</h4>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Item Name</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($invoice->orders as $order)
                    <tr>
                        <td></td>
                        <td>{{  $order->id }}</td>
                        <td>{{ $order->product_name }}</td>
                        <td>${{ $order->sale_price_in_dollar }}</td>
                        <td>{{ $order->product_qty }}</td>
                        <td>${{ $order->sale_price_in_dollar }}</td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div style="clear:both; position:relative;">
            
            <div style="margin-left: 300pt;">
                <h4>Total:</h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td><b>Subtotal</b></td>
                            <td>{{ $invoice->subTotal }}</td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Delivery Charge 
                                </b>
                            </td>
                            <td>{{ $invoice->deliveryCharge }}Tk</td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Bkash Charge 
                                </b>
                            </td>
                            <td>{{ $invoice->bkashCharge }}Tk</td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b>
                                    Weight Charge 
                                </b>
                            </td>
                            <?php $weight_charge =0; ?>
                            <td>@foreach($invoice->orders as $order)
                                <?php $weight_charge += $order->weight_charge; ?>
                                @endforeach
                                {{ $weight_charge  }}
                            Tk</td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Discount 
                                </b>
                            </td>
                            <td>{{ $invoice->discount }}Tk</td>
                        </tr>
                        <tr>
                            <td><b>Advance Paid</b></td>
                            <td><b>{{ $invoice->advancePaid }}Tk</b></td>
                        </tr>
                        <tr>
                            <td><b>Total Due</b></td>
                            <td><b>{{ $totaldue = ($invoice->subTotal+$invoice->bkashCharge-$invoice->discount)-$invoice->advancePaid+$weight_charge }}Tk</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @if ($invoice->footnote)
            <br /><br />
            <div class="well">
                {{ $invoice->footnote }}
            </div>
        @endif
    </body>
</html>