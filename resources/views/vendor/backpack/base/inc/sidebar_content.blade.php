<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a target="_blank" href='{{ url(config('backpack.base.front_route_prefix', 'order') . '/') }}'><i class='fa fa-cog'></i> <span>Orders</span></a></li>
<li><a href="{{ backpack_url('customer') }}"><i class="fa fa-tag"></i> <span>Manage Customer</span></a></li>
<li><a href="{{ backpack_url('brand') }}"><i class="fa fa-tag"></i> <span>Manage Brands</span></a></li>
@role('admin')
<li><a href="{{ backpack_url('status') }}"><i class="fa fa-tag"></i> <span>Manage Status</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>

<!-- Reports -->
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
    	<li><a href="{{ backpack_url('dailyreports') }}"><i class="fa fa-user"></i> <span>Reports</span></a></li>
    </ul>
</li>

<!-- Users, Roles Permissions -->
  <li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ backpack_url('agent') }}"><i class="fa fa-group"></i> <span>Agents</span></a></li>
      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
  </li>
  <li><a href="{{ backpack_url('log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i> <span>Settings</span></a></li>
@endrole