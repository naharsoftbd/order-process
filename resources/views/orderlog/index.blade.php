@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-2">

          <h1 class="daashboard">Dash Board</h1>
          <div class="list-group">
              <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
              <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
               @role('admin')
              <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
              @endrole
              <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
              <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
              <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>
              
          </div>

        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
                  <div class="col-lg-12">
            <div class="panel panel-default">
              <?php //var_dump($orders); ?>
                <div class="panel-heading">
                  <h3> Activity Log</h3>
                </div>
                <div class="panel-body">
                    
                    <div class="row">                      
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th scope="col"></th>
                                <th scope="col">Log Number</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Log User</th>
                                <th scope="col">  </th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Created Time</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($activities as $activity) { ?>
                              <tr>
                                <td></td>
                                <td>{{$activity->id}}</td>                                
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>{{$activity->log_name}}</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>{{$activity->created_at}}</td>
                              </tr>
                            <?php } ?>
                                                         
                           
                            </tbody>
                          </table>
                        </div>
                          {{ $activities->links() }} 
                         
                        </div>
                        <!-- /.col-lg-9 (nested) -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
