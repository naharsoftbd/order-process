@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-2">

          <h1 class="daashboard">Dash Board</h1>
          <div class="list-group">
              <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
              <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
              @role('admin')
              <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
              @endrole
              <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
              <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
              <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>
              
          </div>

        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
            <div class="col-lg-12">
            <div class="panel panel-default">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-heading">
                  <h3>Create Order</h3>
                  <div class="alert alert-success alert-dismissable custom-success-box d-none" style="margin: 15px;">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <strong> Order Created Successfully. </strong>
                  </div>
                  <div class="alert alert-danger alert-dismissable custom-danger-box" style="display:none"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
                </div>
                <div class="panel-body">
                    <form role="form" id="create-product" class="form-horizontal">
                        <!--<form role="form" id="create-product" class="form-horizontal" action="{{ route('create.order') }}">-->
                        @method('get')
                         @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            
                                <div class="form-group required">
                                    <label class='control-label'>Customer name </label>
                                    <select id="e2_2" name="customer-id" class="form-control select2" >
                                    <optgroup label="Customer Name">
                                        <option value="">Select Customer</option>
                                        @foreach($customers as $customer)
                                       <option value="{{$customer->id}}">{{$customer->customer_name}}</option> 
                                       @endforeach
                                      </optgroup>
                                      <optgroup label="Mobile ">
                                        <option value="">Select Customer</option>
                                        @foreach($customers as $customer)
                                       <option value="{{$customer->id}}">{{$customer->mobile}}</option> 
                                       @endforeach
                                      </optgroup>
                                    </select>
                                    <!--<input class="form-control" placeholder="Customer Name" name="CutomerName" required >-->
                                   
                                </div>
                                
                                <div class="form-group required">
                                    <label class='control-label'>Product Name</label>
                                    <input class="form-control" placeholder="Enter Your Product name" name="ProductName" required >
                                </div>
                                <div class="form-group required">
                                    <label class='control-label'>Product Link</label>
                                    <input class="form-control" placeholder="Enter Your Product Link" name="ProductLink" required >
                                </div>
                                <div class="form-group">
                                    <label>Product shade/Comment</label>
                                    <input class="form-control" placeholder="Enter Your Product Comment" name="product_comment">
                                </div>
                                <div class="form-group required">
                                    <label class='control-label'>Brand</label>
                                    <select class="form-control select2" multiple="multiple" name="BuyingPlace[]" required>
                                        <option value="">--</option>
                                        @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                                <div class="form-group">
                                    <label class='control-label'>Quantity</label>
                                    <input class="form-control" placeholder="Enter Product Quantity" name="Quantity" required >
                                </div>
                                <div class="form-group">
                                    <label>Sale By</label>
                                    <select class="form-control" name="SalesBy">
                                        
                                        <option value="{{ $users->id }}">{{ $users->name }}</option>
                                        

                                    </select>
                                    
                                </div>

                                <div class="form-group">
                                    <label>Purchased By</label>
                                    <select class="form-control select2" name="PurchasedBy">
                                        <option value="" >--Select Purchased By--</option>
                                        @foreach($pusers as $puser)
                                        
                                        <option value="{{ $puser->id }}">{{$puser->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                    <!---->
                                </div>
                                <div class="form-group">
                                        <label>Card Last 4 Digit</label>
                                        <input class="form-control" placeholder="Enter Card Last 4 Digit" name="Card-Last-4-Digit">
                                </div>
                                <div class="form-group">
                                        <label>Weight Charge</label>
                                        <input class="form-control" placeholder="Enter Weight Charge" name="weight-charge">
                                </div>
                                
                                                               
                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">

                                <div class="form-group required">
                                    <label class='control-label'>Sale Price in dollar</label>
                                    <input class="form-control" placeholder="Enter Your Sale Price in Dollar" name="Sale-Price-in-dollar" required >
                                </div>
                                    
                                    <div class="form-group">
                                        <label>Actual Buying Price</label>
                                        <input class="form-control" placeholder="Enter Your Actual Buying Price" name="Actual-Buying-Price">
                                    </div>
                                                                       
                                    <!--<div class="form-group">
                                        <label>Total Order Amount</label>
                                        <input class="form-control" placeholder="Enter Total Order Amount" name="Total-Order-Amount" required >
                                    </div>-->
                                    <div class="form-group">
                                        <label>Bkash Receive</label>
                                        <input class="form-control" placeholder="Enter Bkash Receive Amount" name="Bkash-Receive">
                                    </div>

                                    <div class="form-group">
                                        <label>Bkash Charge</label>
                                        <!--<input class="form-control" placeholder="Enter Your Profit" name="Bkash-Charge">-->
                                        <select name="Bkash-Charge" class="form-control">
                                            <option value="">Bkash Charge</option>
                                            <option value="2">Personal</option>
                                            <option value="1.5">Marchent</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label>Bkash Number</label>
                                    <input class="form-control" placeholder="Bkash Number" Name="BkashLast4Digit" >
                                    </div>
                                    <div class="form-group">
                                        <label>Cash Receive</label>
                                        <input class="form-control" placeholder="Enter Cash Receive" name="Cash-Receive">
                                    </div>

                                    <div class="form-group CashReceiveDate">
                                        <label>Cash Receive Date</label>
                                        <input class="form-control" placeholder="Enter Cash Receive Date" Name="CashReceiveDate">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Advance Adjusted</label>
                                        @role('admin')
                                        <select class="form-control select2" name="Advance-Adjusted" required>
                                        @else
                                        <select class="form-control select2" name="Advance-Adjusted" required disabled="true">
                                        @endrole
                                            <option>--Select--</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">NO</option>
                                        </select>
                                    </div>
                                                                        
                                    <div class="form-group">
                                        <label>Delivery Charge</label>
                                        <input class="form-control" placeholder="Enter Delivery Charge" name="For-Courier-charge">
                                    </div>
                                    
                                    <div class="form-group">
                                    <label>Order Status</label>
                                    <select Name="OrderStatus" class="form-control select2">
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="form-group required">
                                    <label class='control-label'>Site</label>
                                    <select Name="OrderAgent" class="form-control select2" required>
                                        <option value=""></option>
                                        @foreach($agents as $agent)
                                        <option value="{{$agent->id}}">{{$agent->name}}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    

                                    <button type="submit" class="btn btn-default">Submit</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                            
                                </div>
                            
                        </div>
                        </form>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

