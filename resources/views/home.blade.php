@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
                   <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h3>  Order Entry Form</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form">
                                <div class="form-group">
                                    <label>Customer name </label>
                                    <input class="form-control" placeholder="Customer Name" name="CutomerName" required >
                                   
                                </div>
                                <div class="form-group">
                                    <label>Contact Number </label>
                                    <input class="form-control" placeholder="Contact Number" name="ContactName" required >
                                   
                                </div>
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" placeholder="Enter Your Product name" name="ProductName" required >
                                </div>
                                <div class="form-group">
                                    <label>Product Link</label>
                                    <input class="form-control" placeholder="Enter Your Product Link" name="ProductLink" required >
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <input class="form-control" placeholder="Enter Product Quantity" name="Quantity" required >
                                </div>
                                <div class="form-group">
                                    <label>Sale By</label>
                                    <input class="form-control" placeholder="Enter Sales Man name" Name="SalesBy" required >
                                </div>

                                <div class="form-group">
                                    <label>Purchased By</label>
                                    <input class="form-control" placeholder="Enter Purchased Man name" Name="PurchasedBy" required >
                                </div>
                                <div class="form-group">
                                    <label>Order Status</label>
                                    <input class="form-control" placeholder="Order status" Name="OrderStatus" required >
                                </div>
                                <div class="form-group">
                                    <label>Buying Place</label>
                                    <input class="form-control" placeholder="Buying Place" Name="BuyingPlace" required >
                                </div>
                                <div class="form-group">
                                    <label>Bkash Last 4 Digit</label>
                                    <input class="form-control" placeholder="4 Digit Bkash" Name="BkashLast4Digit" >
                                </div>


                                
                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">

                                <div class="form-group">
                                    <label>Sale Price in dollar</label>
                                    <input class="form-control" placeholder="Enter Your Sale Price in Dollar" name="Sale-Price-in-dollar" required >
                                </div>
                                    <div class="form-group">
                                        <label>Buying Price in dollar</label>
                                        <input class="form-control" placeholder="Enter Your Buying Price in dollar" name="Buying-Price-in-dollar" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Actual Buying Price</label>
                                        <input class="form-control" placeholder="Enter Your Actual Buying Price" name="Actual-Buying-Price" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Profit Percentage (%)</label>
                                        <input class="form-control" placeholder="Enter Your Profit" name="Profit-Percentage" required >
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Total Order Amount</label>
                                        <input class="form-control" placeholder="Enter Total Order Amount" name="Total-Order-Amount" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Bkash Receive</label>
                                        <input class="form-control" placeholder="Enter Bkash Receive Amount" name="Bkash-Receive" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Advance Adjusted</label>
                                        <select class="form-control" name="Advance-Adjusted" required>
                                            <option>--Select--</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No" >NO</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Card Last 4 Digit</label>
                                        <input class="form-control" placeholder="Enter Card Last 4 Digit" name="Card-Last-4-Digit" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Cash Receive</label>
                                        <input class="form-control" placeholder="Enter Cash Receive" name="Cash-Receive" required >
                                    </div>

                                    <div class="form-group">
                                        <label>For Courier charge</label>
                                        <input class="form-control" placeholder="Enter For Courier charge" name="For-Courier-charge" required >
                                    </div>

                                    <button type="submit" class="btn btn-default">Submit</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                            
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
