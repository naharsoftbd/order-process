<form role="form" id="updatorder" action ="http://localhost/orderprocess/public/edit-order" class="form-horizontal">
   @csrf
   <div class="row">
    <div class="col-lg-6">
        <input type="hidden" id="updatorderid" name="id" value="<?php echo $editorder->id; ?>">
        <div class="form-group">
            <label>Customer name </label>
            <select name="customer-id" class="form-control select2" @role('admin') @else readonly @endrole>
                <option value="">Select Customer</option>
                @foreach($customers as $customer)
                <option  value="{{$customer->id}}" @if($editorder->customer->id==$customer->id) selected @endif>{{$customer->customer_name}}</option> 
                @endforeach
            </select>
            <!--<input class="form-control" placeholder="Customer Name" name="CutomerName" value="<?php echo $editorder->customer->customer_name; ?>" required >-->
            
        </div>
        <div class="form-group">
            <label>Contact Number </label>
            <input class="form-control" placeholder="Contact Number" name="ContactName" value="<?php echo $editorder->customer->mobile; ?>"  required @role('admin') @else readonly @endrole>
            
        </div>
        <div class="form-group">
            <label>Product Name</label>
            <input class="form-control" placeholder="Enter Your Product name" value="<?php echo $editorder->product_name; ?>" name="ProductName" required @role('admin') @else readonly @endrole>
        </div>
        <div class="form-group">
            <label>Product Link</label>
            <input class="form-control" placeholder="Enter Your Product Link" value="<?php echo $editorder->product_link; ?>"  name="ProductLink" required @role('admin') @else readonly @endrole>
        </div>

        <div class="form-group">
            <label>Product shade/Comment</label>
            <input class="form-control" placeholder="Enter Your Product Comment" value="<?php echo $editorder->product_comment; ?>" name="product_comment" required @role('admin') @else readonly @endrole>
        </div>
        <div class="form-group">
            <label>Brands</label>
            <select class="form-control select2" multiple="multiple" name="BuyingPlace[]" required @role('admin') @else readonly @endrole>
                <option value="">--</option>
                @foreach($Brands as $brand)
                @foreach($editorder->brands as $ebrand)
                <option @if($ebrand->id==$brand->id) selected="selected" @endif value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Quantity</label>
            <input class="form-control" placeholder="Enter Product Quantity" name="Quantity" value="<?php echo $editorder->product_qty; ?>" required @role('admin') @else readonly @endrole>
        </div>
        <div class="form-group">
            <label>Sale By</label>
            <select class="form-control" name="SalesBy" readonly>
             <option value="{{$editorder->user->id}}">{{$editorder->user->name}}</option>
         </select>
         <!--<input class="form-control" placeholder="Enter Sales Man name" Name="SalesBy" value="<?php echo $editorder->saleBy->name; ?>" required >-->
     </div>

     <div class="form-group">
        <label>Purchased By</label>
        <select class="form-control" name="PurchasedBy">
            <option value="" >--Select Purchased By--</option>
            @foreach($pusers as $puser)
            <option value="{{ $puser->id }}" 
             <?php  if(!empty($editorder->purchasedBy->id) && $editorder->purchasedBy->id===$puser->id) { ?>selected <?php } ?>>{{$puser->name}}
         </option>
         @endforeach
     </select>

 </div>
 <div class="form-group">
    <label>Card Last 4 Digit</label>
    <input class="form-control" placeholder="Enter Card Last 4 Digit" name="Card-Last-4-Digit" value="<?php echo $editorder->card_last_4digit; ?>" required >
</div>


<div class="form-group">
    <label>Weight Charge</label>
    <input class="form-control" value="<?php echo $editorder->weight_charge; ?>" placeholder="Enter Weight Charge" name="weight-charge">
</div>

<div class="form-group">
    <label>Sale Price in dollar</label>
    <input class="form-control" placeholder="Enter Your Sale Price in Dollar" name="Sale-Price-in-dollar" value="<?php echo $editorder->sale_price_in_dollar; ?>" required @role('admin') @else readonly @endrole>
</div>

</div>
<!-- /.col-lg-6 (nested) -->
<div class="col-lg-6">



    <div class="form-group">
        <label>Actual Buying Price</label>
        <input class="form-control" placeholder="Enter Your Actual Buying Price" name="Actual-Buying-Price" value="<?php echo $editorder->actual_buying_price; ?>" required @role('admin') @else readonly @endrole>
    </div>
    
    <div class="form-group">
        <label>Bkash Receive</label>
        <input class="form-control" placeholder="Enter Bkash Receive Amount" name="Bkash-Receive" value="<?php echo $editorder->bkash_receive; ?>" required @role('admin') @else readonly @endrole>
    </div>
    <div class="form-group">
        <label>Bkash Charge</label>
        <input class="form-control" placeholder="Enter Your Profit" name="Bkash-Charge" value="<?php echo $editorder->bkash_charge; ?>" required @role('admin') @else readonly @endrole>
    </div>
    <div class="form-group">
        <label>Bkash Last 4 Digit</label>
        <input class="form-control" placeholder="4 Digit Bkash" Name="BkashLast4Digit" value="<?php echo $editorder->bkash_l4digit; ?>" required @role('admin') @else readonly @endrole>
    </div>
    @role('admin')
    <div class="form-group">
        <label>Advance Adjusted</label>
        <select class="form-control select2 Advance-Adjusted" name="Advance-Adjusted"  required  >
            <option>--Select--</option>
            <option <?php if($editorder->advance_adjusted=='Yes'){ echo 'selected'; } ?> value="Yes">Yes</option>
            <option value="No" <?php if($editorder->advance_adjusted=='No'){ echo 'selected'; } ?> >NO</option>
        </select>
        
    </div>
    @endrole
    <div class="form-group AdvanceAdjustedDate">
        <label>Advance Adjusted Date</label>
        <input class="form-control" placeholder="Advance Adjusted Date" Name="advanceadjusteddate" value="<?php echo $editorder->advanceadjusteddate; ?>" required>
    </div>
    
    <div class="form-group">
        <label>Cash Receive</label>
        <input class="form-control" placeholder="Enter Cash Receive" name="Cash-Receive" value="<?php echo $editorder->cash_receive; ?>" required >
    </div>
    <div class="form-group CashReceiveDate">
        <label>Cash Receive Date</label>
        <input class="form-control"  value="<?php echo $editorder->cashreceivedate; ?>" placeholder="Enter Cash Receive Date" Name="CashReceiveDate">
    </div>

    <div class="form-group">
        <label>Delivery Charge</label>
        <input class="form-control" placeholder="Enter Delivery Charge" name="For-Courier-charge" value="<?php echo $editorder->for_courier_charge; ?>" required >
    </div>
    
    <div class="form-group">
        <label>Order Status</label>
        <select Name="OrderStatus" class="form-control">
            @foreach($statuses as $status)
            <option value="{{$status->id}}"
                @if($editorder->status->id===$status->id) selected @endif>{{$status->status}}</option>
                @endforeach
            </select>
        </div>
        @role('admin')
        <div class="form-group">
            <label>Shipping Date</label>
            <input class="form-control shipping_at" placeholder="Enter Shipping Date" name="shipping_at" value="<?php echo $editorder->shipping_at; ?>" required >
        </div>
        @endrole
        <div class="form-group">
            <label>Site</label>
            
            <select Name="OrderAgent" class="form-control select2" required>
                <option value=""></option>
                @foreach($agents as $agent)
                <option value="{{$agent->id}}" @if($editorder->agent->id==$agent->id) selected @endif>{{$agent->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
        <label>Due Payment Paied</label>
        <input class="form-control" placeholder="Enter Due Payment" name="paymentpaid" value="<?php echo $editorder->paid; ?>" required @role('admin') @else readonly @endrole>
    </div>
        
    </div>
    <div class="form-group col-sm-12 required">
      <label class='control-label' for="comment">Comment:</label>
      <textarea class="form-control" rows="5" name="edit-comment" id="edit-comment"><?php echo $editorder->remarks; ?></textarea>
  </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
       $('.select2').select2({
          placeholder: 'Select an option'
      });

       $('.shipping_at').datepicker({
          'dateFormat':'yy-mm-dd',
          yearRange: "-10:+20",
      });

   })

</script>