
<div class="row">
    <div class="col-md-6">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center">Order ID#</th>
                <th style="text-align: center">Site</th>
                <th style="text-align: center">Sale By</th>
                <th style="text-align: center">Product Name</th>
                <th style="text-align: center">Total</th>
                <th style="text-align: center">Advance Adjusted</th>
                <th style="text-align: center">Order Date</th>
            </tr>
            </thead>
            <tbody>
                <?php $total=0; //var_dump($orders); ?>
                @foreach($orders as $order)
                <tr>
                    <td style="text-align: center">{{$order->id}}</td>
                    <td style="text-align: center">{{ $order->agent->name }}</td>
                    <td style="text-align: center">{{ $order->saleBy->name }}</td>
                    <td style="text-align: center">{{$order->product_name}}</td>
                    <td style="text-align: center">Tk {{$order->total_order_amount}}</td>
                    <?php $total+=$order->total_order_amount; ?>
                    <td style="text-align: center">{{$order->advance_adjusted}}</td>
                    <td style="text-align: center">{{$order->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered">
            <thead>
            <tr style="font-size:14px">
                <!--<th style="text-align: center">Period</th>-->
                <th style="text-align: center">Total</th>
            </tr>
            </thead>
            <tbody>
                
                <tr>
                    
                    <td style="text-align: center">Tk {{floatval($total)}}</td>
                </tr>
                
            </tbody>
        </table>
    </div>
</div>
{{ $orders->links() }} 
   
    <script>
        $( document ).ready(function() {
             var siteurl = '<?php echo url('/admin'); ?>';
           $( ".report_from" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            onOpen: function () {
            old_date = $(this).val();
        },
            onClose: function () {
                if ($(this).val()){
                    $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           //console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })
                
            }
            }
           });
           $('.advanceadjusted').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.report-bysite').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.report-saleby').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.filterreport').on('click',function(){
                //console.log($('#report_from').val());
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                            //console.log(data);
                               $('.reports').empty();
                             $('.reports').html(data);
                        }
                    })

           });
});
    
    
</script>
