@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        Report<small>Daily Summary Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">Report</a></li>
        <li class="active">Daily Summary Report</li>
      </ol>
    </section>
@endsection


@section('content')
<h2 class="page-header"></h2>
<div class="row">
    <form method="POST" action="{{ url('/')}}daily-summary">
        <input type="hidden" name="_method" value="POST">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-sm-3 form-group">
        <label>From Date</label>
      <div class='input-group date'>
        <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
        </span>
                    <input type='text' class="form-control report_from" value="{{date('Y-m-d')}}"
                           id="report_from"/>
        </div>
    </div>
    <div class="col-sm-3 form-group">
      <label>Last Date</label>
      <div class='input-group date'>
        <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
        </span>
                    <input type='text' class="form-control report_from" value="{{date('Y-m-d')}}"
                           id="report_to" name="report_to"/>
        </div>
    </div>
    <div class="col-sm-3 form-group">
        <label>Shipping Date</label>
      <div class='input-group date'>

        <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
        </span>
                    <input type='text' class="form-control report_from" value="{{date('Y-m-d')}}"
                           id="csvshipping" name="csvshipping"/>
        </div>
    </div>
    <div class="form-group col-sm-3">
        <label>Advance Adjusted</label>
        <select class="form-control advanceadjusted" name="advanceadjusted">
            <option value="">Adjustment</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
    <div class="form-group col-sm-3">
        <select class="form-control report-bysite" name="report-bysite">
            <option value="">Select Site</option>
            @foreach($agents as $agent)
            <option value="{{$agent->id}}">{{$agent->name}}</option>
            @endforeach
            
        </select>
    </div>
    <div class="form-group col-sm-3">
        <select class="form-control report-saleby" name="report-saleby">
            <option value="">Sale By</option>
            @foreach($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
            
        </select>
    </div>
    <input type="button" value="Filter" class="filterreport btn btn-default">
    <button type="reset" class="btn btn-default">Reset</button>
</form>
</div>
<h2 style="text-align: center;padding: 10px;background: whitesmoke">
   </h2>
<div class="reports">
<div class="row ">
    <div class="col-md-6">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center">Order ID#</th>
                <th style="text-align: center">Site</th>
                <th style="text-align: center">Sale By</th>
                <th style="text-align: center">Product Name</th>
                <th style="text-align: center">Total</th>
                <th style="text-align: center">Advance Adjusted</th>
                <th style="text-align: center">Order Date</th>
            </tr>
            </thead>
            <tbody>
                <?php $total=0; //var_dump($orders); ?>
                @foreach($orders as $order)
                <tr>
                    <td style="text-align: center">{{$order->id}}</td>
                    <td style="text-align: center"><?php if(!empty($order->agent->name)){ ?> {{  $order->agent->name  }} <?php } ?></td>
                    <td style="text-align: center">{{ $order->saleBy->name }}</td>
                    <td style="text-align: center">{{$order->product_name}}</td>
                    <td style="text-align: center">Tk {{$order->total_order_amount}}</td>
                    <?php $total+=$order->total_order_amount; $date=date('Y-m-d', strtotime($order->created_at)); ?>
                    <td style="text-align: center">{{$order->advance_adjusted}}</td>
                    <td style="text-align: center">{{$order->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered">
            <thead>
            <tr style="font-size:14px">
                <!--<th style="text-align: center">Period</th>-->
                <th style="text-align: center">Total</th>
            </tr>
            </thead>
            <tbody>
                
                <tr>
                    <!--<td style="text-align: center">{{$date}}</td>-->
                    <td style="text-align: center">Tk {{ floatval($total) }}</td>
                </tr>
                
            </tbody>
        </table>
    </div>

    </div>
   {{ $orders->links() }}  
</div>
    <script>
        
        $( document ).ready(function() {
            var siteurl = '<?php echo url('/admin'); ?>';
           $( ".report_from" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            onOpen: function () {
            old_date = $(this).val();
        },
            onClose: function () {
                if ($(this).val()){
                    $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           //console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })
                
            }
            }
           });
           $('.advanceadjusted').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.report-bysite').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.report-saleby').on('change',function(){
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                           // console.log(data);
                               $('.reports').empty();
                                $('.reports').html(data);
                        }
                    })

           });
           $('.filterreport').on('click',function(){
                //console.log($('#report_from').val());
                $.ajax({
                        method:'POST',
                        url: siteurl+'/daily-summary',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{'report_from':$('#report_from').val(),'report_to':$('#report_to').val(),'advanceadjusted':$('.advanceadjusted').val(),'csvshipping':$('#csvshipping').val(),'site':$('.report-bysite').val(),'saleby':$('.report-saleby').val()},
                        success:function(data){
                            //console.log(data);
                               $('.reports').empty();
                             $('.reports').html(data);
                        }
                    })

           });
         });
           
    
    
</script>


@endsection
