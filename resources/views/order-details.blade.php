@extends('layouts.app')

@section('content')
<div class="container-fluid">
 <div class="row justify-content-center">
  <div class="col-lg-2">

   <h1 class="daashboard">Dash Board</h1>
   <div class="list-group">
    <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
    <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
    @role('admin')
    <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
    @endrole
    <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
    <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
    <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>

   </div>

  </div>
  <div class="col-md-10">
   <div class="card">
    <div class="card-header">Dashboard</div>

    <div class="card-body">
     <div class="col-lg-12">
      <div class="panel panel-default">
       <div class="panel-heading">
        <h3>  Order Details</h3>
       </div>
       <div class="panel-body">
        <div class="row">                      
         <table class="table table-striped">
          <thead>
           <tr>
            <th scope="col">Field Name</th>
            <th scope="col">Value Name</th>
           </tr>
          </thead>
          <tbody>
            <tr>
            <th scope="row">Site</th>
            <td>{{$orders->agent['name']}}</td>
           </tr>
            
           <tr>
            <th scope="row">Order Number</th>
            <td><?php echo $orders->id; ?></td>
           </tr>
           <tr>
            <th scope="row">Product Name</th>
            <td><?php $productArray = explode(',', $orders->product_name);  foreach($productArray as $product){ ?><?php echo $product; ?>&nbsp;<?php } ?></td>
           </tr>

           <tr>
            <th scope="row">Product Links</th>
            
            <td><?php $linkArray = explode(',', $orders->product_link);  foreach($linkArray as $link){ ?><a target="_blank" href="<?php echo $link ?>">   <?php echo $link; ?></a>  &nbsp;<?php } ?></td>
            
           </tr>
           <tr>
            <th scope="row">Product Shed/Comment</th>
            <td><?php echo $orders->product_comment; ?></td>
           </tr>
           <tr>
            <th scope="row">Customer name </th>
            <td><?php echo $orders->customer->customer_name;; ?></td>
           </tr>
           <tr>
            <th scope="row">Contact Number </th>
            <td><?php echo $orders->customer->mobile; ?></td>
           </tr>
           <tr>
            <th scope="row">Quantity </th>
            <td><?php echo $orders->product_qty; ?></td>
           </tr>
           <tr>
            <th scope="row">Sale By </th>
            <td><?php echo $orders->saleBy->name ?></td>
           </tr>
           <tr>
            <th scope="row">Purchased By </th>
            <td><?php if(!empty($orders->purchasedBy->name)){echo $orders->purchasedBy->name; }?></td>
           </tr>
           <tr>
            <th scope="row">Order Status </th>
            <td><?php echo $orders->status->status; ?></td>
           </tr>
           <tr>
            <th scope="row">Shipping Date </th>
            <td><?php echo $orders->shipping_at; ?></td>
           </tr>
           <tr>
            <th scope="row">Brands</th>
            <td>
             @foreach($orders->brands as $brand)
             {{ $loop->first ? '' : ', ' }}
             {{$brand->name}}
             @endforeach

            </td>
           </tr>

           <tr>
            <th scope="row">Sale Price in dollar </th>
            <td>$<?php echo $orders->sale_price_in_dollar; ?></td>
           </tr>

           <tr>
            <th scope="row">Actual Buying Price</th>
            <td>$<?php echo $actbprice = floatval($orders->actual_buying_price); ?></td>
           </tr>
           <tr>
            <th scope="row">Profit</th>
            <td>$<?php
            $sale_price = floatval($orders->sale_price_in_dollar);
            echo $profit = $sale_price-$actbprice;
            ?></td>
           </tr>
           <tr>
            <th scope="row">Profit Percentage (%)</th>
            <td><?php echo $profitper = ($profit/ $sale_price)*100; ?>%</td>
           </tr>
           <tr>
            <th scope="row">Total Order Amount</th>
            <td><?php  

            $dollar = floatval(Config::get('settings.dollar')); 

            echo $totalamount = $sale_price * $dollar;

            ?> tk</td>
           </tr>
           <tr>
            <th scope="row">Bkash Receive</th>
            <td><?php echo $orders->bkash_receive; ?> tk</td>
           </tr>
           <tr>
            <th scope="row">Bkash Charge</th>
            <td><?php echo $orders->bkash_charge; ?> tk</td>
           </tr>
           <tr>
            <th scope="row">Bkash Number </th>
            <td><?php echo $orders->bkash_l4digit; ?></td>
           </tr>
           <tr>
            <th scope="row">Advance Adjusted</th>
            <td><?php echo $orders->advance_adjusted; ?></td>
           </tr>
           <tr>
            <th scope="row">Advance Adjusted Date</th>
            <td><?php echo $orders->advanceadjusteddate; ?></td>
           </tr>
           <tr>
            <th scope="row">Card Last 4 Digit</th>
            <td><?php echo $orders->card_last_4digit; ?></td>
           </tr>
           <tr>
            <th scope="row">Cash Receive</th>
            <td><?php echo $orders->cash_receive; ?>tk</td>
           </tr>
           <tr>
            <th scope="row">Cash Receive Date</th>
            <td><?php echo $orders->cashreceivedate; ?></td>
           </tr>
           <tr>
            <th scope="row">Delivery Charge</th>
            <td><?php echo $orders->for_courier_charge; ?>tk</td>
           </tr>
           <tr>
            <th scope="row">Weight Charge</th>
            <td>$<?php echo $orders->weight_charge; ?> USD</td>
           </tr>
           <tr>
            <th scope="row">Edit Comments</th>
            <td><?php echo $orders->remarks; ?></td>
           </tr>
           <tr>
            <th style="color:chartreuse; font-weight:bold; background: cornflowerblue;" scope="row"> Total amount</th>
            <td style="color:chartreuse; font-weight:bold; background: cornflowerblue;"> <?php echo $totalamount = $totalamount+$orders->bkash_charge; ?> tk</td>
           </tr>
           <tr>
            <th scope="row">Total Paid amount</th>
            <td><h3><?php echo $paidamount = floatval($orders->bkash_receive)+floatval($orders->cash_receive)+floatval($orders->paid); ?> tk</h3></td>
           </tr><tr>
            <th scope="row">Bkash Charge</th>
            <td><h3><?php echo $orders->bkash_charge; ?> tk</h3></td>
           </tr>    
           <tr>
            <th style="color:brown; font-weight:bold" scope="row">Due amount</th>
            <td style="color:brown; font-weight:bold ">
             <?php echo $dueamount = $totalamount-$paidamount; ?> tk</td>
            </tr>



           </tbody>
          </table>
         </div>
        </form>
       </div>
       <!-- /.col-lg-9 (nested) -->
      </div>
      <!-- /.row (nested) -->
     </div>
     <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
   </div>

  </div>
 </div>
</div>
</div>
</div>
@endsection
