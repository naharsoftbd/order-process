@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-lg-2">

      <h1 class="daashboard">Dash Board</h1>
      <div class="list-group">
        <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
        <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
        @role('admin')
        <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
        @endrole
        <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
        <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
        <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>
        
      </div>

    </div>
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Invoices</div>

        <div class="card-body">
          
          <div class="col-lg-12">
            <div class="panel panel-default">
              <!--<h2>Search By</h2>-->
              <?php  ?>
              <div class="panel-heading">
                <h3>Customer Invoices</h3>
              </div>
              <div class="panel-body">
                <div class="row">  
                 <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th scope="col">Invoice No</th>
                      <th scope="col">Orders</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Sub Total</th>
                      <th scope="col">Bkash Charge</th>
                      <th scope="col">Discount</th>
                      <th scope="col">Advance Paid</th>
                      <th scope="col">Total</th>
                      <th scope="col">Download</th>
                    </tr>
                  </thead>
                  <tbody>  
                    @foreach($invoices as $invoice)
                    <tr>
                      <td><a href="{{route('invoicedetails',['id'=>$invoice->id])}}">#{{ $invoice->id }}</a></td>                     
                     
                     <td>@foreach($invoice->orders as $order)
                      <a href="{{route('orders.details',['id'=>$order->id])}}">
                        
                        {{ $loop->first ? '' : ', ' }}
                        {{ $order->product_name}}</a>
                        @endforeach
                      </td>
                      <td>@foreach($invoice->orders as $order)                               
                       
                        {{ $loop->first ? '' : ', ' }}
                        {{ $order->product_qty}}
                      @endforeach</td>
                      <td><a href="">Tk {{ $invoice->subTotal }}</a></td>
                      <td><a href="">Tk {{ $invoice->bkashCharge }}</a></td>
                      <td><a href="">Tk {{ $invoice->discount }}</a></td>
                      <td><a href="">Tk {{ $invoice->advancePaid }}</a></td>
                      <td><a href="">Tk {{ $total = $invoice->subTotal- $invoice->discount }}</a></td>
                      <td><a class="btn btn-success" href="{{route('downloadinvoice',['id'=>$invoice->id])}}">Download</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection