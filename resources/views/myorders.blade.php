@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-lg-2">

      <h1 class="daashboard">Dash Board</h1>
      <div class="list-group">
        <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
        <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
        @role('admin')
        <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
        @endrole
        <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
        <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
        <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>

      </div>

    </div>
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">

          <div class="col-lg-12">
            <div class="panel panel-default">
              <!--<h2>Search By</h2>-->
              <?php  ?>
              <div class="panel-heading">
                <h3> Orders List</h3>
              </div>
              <div class="panel-body">
                <form id="frm-example"  method="POST">
                  <div class="row">                      
                    <table class="table table-striped  my-orders">
                      <thead>

                        <tr>
                          <?php 
                          $page = null;
                          $uri =  $_SERVER["REQUEST_URI"]; //it will print full url
                                  $uriArray = explode('/', $uri); //convert string into array with explode
                                   if(!empty($uriArray[5])){
                                    $id = $uriArray[5]; 
                                    $ret = explode('?', $id);
                                    $page = $ret[0];
                                   }
                                   
                                  ?>
                            <div class="col-sm-12 col-md-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="custom-select mycustom-select custom-select-sm form-control form-control-sm">
                              <option value="">--</option>
                              <option <?php if($page==10){echo 'selected'; } ?> value="10">10</option>
                              <option <?php if($page==25){echo 'selected'; } ?> value="25">25</option>
                              <option <?php if($page==50){echo 'selected'; } ?> value="50">50</option>
                              <option <?php if($page==100){echo 'selected'; } ?> value="100">100</option></select> entries</label></div></div>
                          <th scope="col"></th>
                          <th scope="col">Order Number</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Customer Name</th>
                          <th scope="col">Customer Number</th>
                          <th scope="col">Order Status</th>
                          <th scope="col">Sales By</th>
                          <th scope="col">Sale Price</th> 
                          <th scope="col">Buying Price</th>
                          <th scope="col">Profit</th>
                          <th scope="col">Profit(%)</th>
                          <th scope="col">Site</th>    

                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($orders as $order) { ?>
                          <tr>
                            <td scope="row"><?php echo $order->id; ?></td>
                            <th scope="row"><?php echo $order->id; ?></th>
                            <td><?php echo $order->product_name; ?></td>
                            <td><?php echo $order->customer->customer_name; ?></td>
                            <td><?php echo $order->customer->mobile; ?></td>
                            <td><?php echo $order->status->status; ?></td>
                            <td><?php echo $order->saleBy->name ?></td>
                            <td>$ <?php echo $order->sale_price_in_dollar; ?></td>
                            <td>$ <?php echo $actbprice = floatval($order->actual_buying_price); ?></td>
                            <td>
                              $ <?php
                              $sale_price = floatval($order->sale_price_in_dollar);
                                 $profit = $sale_price-$actbprice;
                                echo  round($profit,2);
                              ?>

                            </td>
                            <td><?php $profitper = ($profit/ $sale_price)*100; echo  round($profitper,2); ?> %</td>
                            <td>{{$order->agent['name']}}</td>
                            <td class="order-list-table">
                              <a  href="<?php echo route('orders.details',['id'=>$order->id]);?>" type="button"  class="btn btn-primary button12">View</a>
                              <?php if(backpack_user()->hasRole('admin')){ ?>
                                <a data-id="<?php echo $order->id; ?>" type="button" data-toggle="modal" data-target="#confirm-edit"  class="btn btn-success button12 confirm-edit">Edit</a>
                                <a type="button" data-toggle="modal" data-target="#confirm-delete" data-id="<?php echo $order->id; ?>" class="btn btn-danger confirm-delete button12">Delete</a>
                              <?php } ?>
                            </td>
                          </tr>
                        <?php } ?>


                      </tbody>
                      <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right">Total:${{$totals}}</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                  </div>
                  {{ $orders->links() }} 
                  <div class="alert alert-success alert-dismissable invoice-success-box d-none" style="margin: 15px;">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   <strong> Invoice Created Successfully. </strong>
                 </div>
                 <div class="form-group d-none discountarea col-md-6">
                      <label>Discount</label>
                      <input type="text" name="discount" class="form-control discount">
                   </div>
                 <input type="button" class="btn btn-success myorder-invoice" name="invoice" value="Generate Invoice">
               </form>
             </div>
             <!-- /.col-lg-9 (nested) -->
           </div>
         </div>
       </div>
     </div>
   </div>
   <!-- Modal -->
   <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
          <p>You are about to delete <b><i class="title"></i></b> record, this procedure is irreversible.</p>
          <p>Do you want to proceed?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger btn-ok">Delete</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Edit-->
  <div class="modal fade" id="confirm-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">Please Save before Closing</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body edit-content">



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-success edit-btn-ok">Save</button>
        </div>
      </div>
    </div>
  </div>
  @endsection
