@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-2">

          <h1 class="daashboard">Dash Board</h1>
          <div class="list-group">
              <a href="<?php echo URL::to('/order'); ?>" class="list-group-item">Create Order</a>
              <a href="<?php echo URL::to('/orders'); ?>" class="list-group-item">Orders</a>
              @role('admin')
              <a href="<?php echo URL::to('/activitylog'); ?>" class="list-group-item">Activitylog</a>
              @endrole
              <a href="<?php echo URL::to('/my-orders'); ?>" class="list-group-item">My Orders</a>
              <a href="<?php echo URL::to('/my-customers'); ?>" class="list-group-item">My Customers</a>
              <a href="<?php echo URL::to('/invoices'); ?>" class="list-group-item">My Customers Invoices</a>
              
          </div>

        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
                  <div class="col-lg-12">
            <div class="panel panel-default">
              <?php //var_dump($orders); ?>
                <div class="panel-heading">
                  <h3>Customers </h3>
                </div>
                <div class="panel-body">
                    <div class="row">                      
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                
                                <th scope="col">Customer Name</th>
                                <th scope="col">Customer Number</th>
                                <th scope="col">Customer Email</th>

                                <th scope="col">Orders</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($customers as $customer) { ?>
                              <tr>
                            
                                <td><?php echo $customer->customer_name; ?></td>
                                <td><?php echo $customer->mobile; ?></td>
                                <td><?php echo $customer->email; ?></td>
                                <td><a href="<?php echo route('customer.order',['id' => $customer->id]); ?>"><?php echo $customer->orders->count(); ?></a></td>
                                                                
                              </tr>
                            <?php } ?>
                                                         
                           
                            </tbody>
                          </table>
                                </div>
                          {{ $customers->links() }} 
                        </div>
                        <!-- /.col-lg-9 (nested) -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>You are about to delete <b><i class="title"></i></b> record, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger btn-ok">Delete</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Edit-->
<div class="modal fade" id="confirm-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <h4 class="modal-title" id="myModalLabel">Please Save before Closing</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body edit-content">
                    
                        
                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success edit-btn-ok">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection
