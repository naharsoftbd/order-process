<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Status;
use App\Customer;
use App\Brand;
use App\Agent;

class Order extends Model
{
   use CrudTrait;
    public function status(){
    	return $this->belongsTo('App\Status','status_id');
    }

    public function customer(){
    	return $this->belongsTo('App\Customer','customer_id');
    }
    public function saleBy(){
    	return $this->belongsTo('App\User','sale_by');
    }

     public function purchasedBy(){
    	return $this->belongsTo('App\User','purchased_by');
    }
    public function brands(){
        return $this->belongsToMany(Brand::class);
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function agent(){
        return $this->belongsTo(Agent::class);
    }
}
