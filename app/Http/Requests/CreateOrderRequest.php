<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer-id' => 'required',
            'ProductName' => 'required',
            'ProductLink' => 'required',
            'Quantity' => 'required',
            'SalesBy' => 'required',
            'PurchasedBy' => 'nullable',
            'OrderStatus' => 'required',
            'BuyingPlace' => 'required',
            'BkashLast4Digit' => 'nullable|numeric',
            'product_comment' => 'nullable',
            'Actual-Buying-Price' => 'nullable',
            'Bkash-Receive' => 'nullable',
            'Bkash-Charge' => 'nullable',
            'Card-Last-4-Digit' => 'nullable|digits:4|numeric|min:4',
            'Cash-Receive' => 'nullable',
            'For-Courier-charge' => 'nullable',
            'edit-comment' => 'nullable',
            'CashReceiveDate' => 'nullable',
            'OrderAgent' =>'required',
            'weight-charge' =>'nullable',
            'shipping_at' =>'nullable',
            'paymentpaid' =>'nullable'

        ];
    }

    public function messages()
    {
        return [
            'customer-id.required' => 'Customer Name is required',
            'ContactName.required'  => 'A message is required',
        ];
    }

}
