<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Config;
use App\User;
use App\Status;
use App\Customer;
use App\Brand;
use Carbon\Carbon;
use Spatie\Activitylog\Models\Activity;
use App\Agent;
use Response;



class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('isAdmin');
        $this->middleware(backpack_middleware());
    }
    public function index(){

        return redirect('orders');
    }

    public function createOrder(CreateOrderRequest $request){

        $validated = $request->validated();

        $Order = new Order();
        $Order->customer_id = $request->input('customer-id');
        $Order->cashreceivedate = date("Y-m-d", strtotime($request->input('CashReceiveDate')));
        $Order->product_name = $request->input('ProductName');
        $Order->product_link = $request->input('ProductLink');
        $Order->product_qty = $request->input('Quantity');
        $Order->sale_by = $request->input('SalesBy');
        $Order->purchased_by = $request->input('PurchasedBy');
        $Order->status_id = $request->input('OrderStatus');
        $Order->product_comment = $request->input('product_comment');
        $Order->bkash_l4digit = $request->input('BkashLast4Digit');
        $Order->sale_price_in_dollar = $request->input('Sale-Price-in-dollar');
        $Order->actual_buying_price = $request->input('Actual-Buying-Price');
        $Order->profit = floatval($request->input('Sale-Price-in-dollar')) - floatval($request->input('Actual-Buying-Price'));
        $Order->total_order_amount = floatval($request->input('Sale-Price-in-dollar'))*floatval(Config::get('settings.dollar'));
        $Order->bkash_receive = $request->input('Bkash-Receive');
        $Order->bkash_charge = ($request->input('Bkash-Receive')*$request->input('Bkash-Charge'))/100;
        $Order->advance_adjusted = $request->input('Advance-Adjusted');
        $Order->card_last_4digit = $request->input('Card-Last-4-Digit');
        $Order->cash_receive = $request->input('Cash-Receive');
        $Order->for_courier_charge = $request->input('For-Courier-charge');
        $Order->weight_charge = $request->input('weight-charge');
        $Order->user_id = backpack_user()->id;
        $Order->agent_id = $request->input('OrderAgent');
        $Order->paid = $request->input('paymentpaid');
        $Order->Save();
        if(!empty($request->input('BuyingPlace'))){
            $Order->brands()->sync($request->input('BuyingPlace'));
        }
        

        return url('orders');


    }
    public function getOrder($id){
        
        $order = Order::find($id);
        $customers = Customer::all();
        $pusers = User::getRoleBy('purchase');
        $users = User::getRoleBy('user');
        $statuses = Status::all();
        $agents = Agent::all();
        $Brands = Brand::orderBy('name', 'asc')->get();
        return view('edit',['editorder'=>$order,'customers'=>$customers,'users' => $users,'pusers'=>$pusers,'statuses' => $statuses,'agents'=>$agents,'Brands'=>$Brands]);

    }
    public function getMyOrders(){

      $orders = new Order();

       $orders =   $orders->with('customer','saleBy')->whereUserId(backpack_user()->id)->orderBy('id', 'desc')->paginate(10);

       $balance = $orders->sum('sale_price_in_dollar');
       
       return view('myorders',['orders' => $orders,'totals' =>$balance]);

   }
   public function getMyOrdersPage($id){

        $orders = new Order();
        if(!empty($id)){
            $orders =  $orders->with('customer','saleBy')->whereUserId(backpack_user()->id)->orderBy('id', 'desc')->paginate($id);
        }else{
           $orders =  $orders->with('customer','saleBy')->whereUserId(backpack_user()->id)->orderBy('id', 'desc')->paginate(10); 
        }
        
        $balance = $orders->sum('sale_price_in_dollar');

       return view('myorders',['orders' => $orders,'totals' =>$balance]);

   }

   public function getOrders($id,Request $request)
    {
        $pusers = User::getRoleBy('purchase');
        $users = User::all();
        $statuses = Status::all();
        $customers = Customer::all();
        $brands = Brand::orderBy('name', 'asc')->get();
        $agents = Agent::all();
        $orders = new Order();

        if(!empty($id) && !Request()->has('OrderAgent') ){
          $orders = $orders->with('customer','saleBy','status')->orderBy('id', 'desc'); 
          $balance = $orders->sum('sale_price_in_dollar');
      }

      if (!empty($request['OrderAgent']) && !empty($id)) {
          $orders = $orders->where('agent_id',$request['OrderAgent'])->orderBy('id', 'desc');
          $balance = $orders->sum('sale_price_in_dollar');
      }

      if (!empty($request['mobile']) && !empty($id)) {
          $customer = Customer::where('mobile',$request['mobile'])->first();
          $orders = $orders->where('customer_id',$customer->id)->orderBy('id', 'desc');
      
      }

      if (!empty($request['user_id']) && !empty($id)) {
          $user = User::find($request['user_id']);
          $orders = $orders->where('user_id',$user->id)->orderBy('id', 'desc');
      
      }

       if (!empty($request['datefrom']) && !empty($request['dateto']) && !empty($id)) {
             $from    = Carbon::parse($request['datefrom'])
                 ->startOfDay()        // 2018-09-29 00:00:00.000000
                 ->toDateTimeString(); // 2018-09-29 00:00:00

            $to      = Carbon::parse($request['dateto'])
                 ->endOfDay()          // 2018-09-29 23:59:59.000000
                 ->toDateTimeString(); // 2018-09-29 23:59:59
          $orders = $orders->whereBetween('created_at', [$from, $to])->orderBy('id', 'desc');
      
      }


         $orders = $orders->paginate($id)->appends(['OrderAgent'=>$request['OrderAgent'],'user_id'=>$request['user_id'],'mobile'=>$request['mobile'],'datefrom'=>$request->datefrom,'dateto'=>$request->dateto]);

         $balance = $orders->sum('sale_price_in_dollar');
      
        

         return view('orders',['orders' => $orders, 'users'=>$users,'pusers'=>$pusers,'statuses' => $statuses,'customers'=>$customers,'brands'=>$brands,'agents'=>$agents,'totals' =>$balance]);
    }

   public function updateOrder(CreateOrderRequest $request){

        //$validated = $request->validated();

    $Order = Order::find($request->input('id'));

    $Order->customer_id = $request->input('customer-id');
        //$Order->contact_number = $request->input('ContactName');
    $Order->product_name = $request->input('ProductName');
    $Order->product_link = $request->input('ProductLink');
    $Order->product_qty = $request->input('Quantity');
    $Order->product_comment = $request->input('product_comment');
    $Order->sale_by = $request->input('SalesBy');
    $Order->purchased_by = $request->input('PurchasedBy');
    $Order->status_id = $request->input('OrderStatus');
    $Order->bkash_l4digit = $request->input('BkashLast4Digit');
    $Order->sale_price_in_dollar = $request->input('Sale-Price-in-dollar');
    $Order->actual_buying_price = $request->input('Actual-Buying-Price');
    $Order->profit = floatval($request->input('Sale-Price-in-dollar')) - floatval($request->input('Actual-Buying-Price'));
    $Order->total_order_amount = floatval($request->input('Sale-Price-in-dollar'))*floatval(Config::get('settings.dollar'));
    $Order->bkash_receive = $request->input('Bkash-Receive');
    $Order->bkash_charge = $request->input('Bkash-Charge');
    $Order->advance_adjusted = $request->input('Advance-Adjusted');
    $Order->cashreceivedate = date("Y-m-d", strtotime($request->input('CashReceiveDate')));
    $Order->advanceadjusteddate = date("Y-m-d", strtotime($request->input('advanceadjusteddate')));
    $Order->card_last_4digit = $request->input('Card-Last-4-Digit');
    $Order->cash_receive = $request->input('Cash-Receive');
    $Order->for_courier_charge = $request->input('For-Courier-charge');
    $Order->weight_charge = $request->input('weight-charge');
    $Order->shipping_at = $request->input('shipping_at');
    $Order->user_id = backpack_user()->id;
    $Order->remarks = $request->input('edit-comment');
    $Order->agent_id = $request->input('OrderAgent');
    $Order->paid = $request->input('paymentpaid');
    $Order->Save();
    $Order->brands()->sync($request->input('BuyingPlace'));

    return activity(backpack_user()->name.': #'.$request->input('id'))->log($request->input('ProductName'));

}
public function getOrderByCustomerId($id){
    $orders = Order::with('customer','saleBy')->whereCustomerId($id)->paginate(10);
    return view('myorders',['orders' => $orders]);

}
public function deleteOrder($id){

    activity(backpack_user()->name.' Deleted : Order#'.$id)->log(backpack_user()->name.': #'.$id);
    return $orders = Order::destroy($id);


}

public function getOrderLog(){

    $activities = Activity::orderBy('id','DESC')->paginate(10);
    return view('orderlog.index',['activities'=>$activities]);
}

public function getOrderCsv(Request $reqiest){
    $csvfrom = $reqiest->input('csvfrom');
    $csvto = $reqiest->input('csvto');
    $shippingDate = $reqiest->input('csvshipping');

     $fromDate = new Carbon($csvfrom); 
     $toDate = new Carbon($csvto);
     $shippingDate = new Carbon($shippingDate);
     $table = Order::with('customer','saleBy','status','agent')->whereBetween('created_at', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()))->orWhere('shipping_at',$shippingDate)->orderBy('id', 'desc')->get();
    $filename = "orders.csv";
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('customer_name','product_name', 'product_link', 'sale_by', 'created_at'));

    foreach($table as $row) {
        fputcsv($handle, array($row['product_name'], $row['product_link'], $row->saleBy->name, $row['created_at']));
    }

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'orders.csv', $headers);
}

}
