<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Order;
use App\User;
use App\Status;
use App\Customer;
use App\Brand;
use App\Agent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware(backpack_middleware());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pusers = User::getRoleBy('purchase');
        $users = backpack_user();
        $statuses = Status::all();
        $customers = Customer::all();
        $brands = Brand::orderBy('name', 'asc')->get();
        $agents = Agent::all();
        return view('order',['users'=>$users,'pusers'=>$pusers,'statuses' => $statuses,'customers'=>$customers,'brands'=>$brands,'agents'=>$agents]);
    }
    //list all order
    public function getOrders(Request $request)
    {
        $pusers = User::getRoleBy('purchase');
        $users = User::all();
        $statuses = Status::all();
        $customers = Customer::all();
        $brands = Brand::orderBy('name', 'asc')->get();
        $agents = Agent::all();
        $orders = new Order();

        if( !Request()->has('OrderAgent') ){
          $orders = $orders->with('customer','saleBy','status')->orderBy('id', 'desc'); 
          $balance = $orders->sum('sale_price_in_dollar');
      }

      if (!empty($request['OrderAgent'])) {
          $orders = $orders->where('agent_id',$request['OrderAgent'])->orderBy('id', 'desc');
          $balance = $orders->sum('sale_price_in_dollar');
      }

      if (!empty($request['mobile'])) {
          $customer = Customer::where('mobile',$request['mobile'])->first();
          $orders = $orders->where('customer_id',$customer->id)->orderBy('id', 'desc');
      
      }

      if (!empty($request['user_id']) ) {
          $user = User::find($request['user_id']);
          $orders = $orders->where('user_id',$user->id)->orderBy('id', 'desc');
      
      }

       if (!empty($request['datefrom']) && !empty($request['dateto'])) {
             $from    = Carbon::parse($request['datefrom'])
                 ->startOfDay()        // 2018-09-29 00:00:00.000000
                 ->toDateTimeString(); // 2018-09-29 00:00:00

            $to      = Carbon::parse($request['dateto'])
                 ->endOfDay()          // 2018-09-29 23:59:59.000000
                 ->toDateTimeString(); // 2018-09-29 23:59:59
          $orders = $orders->whereBetween('created_at', [$from, $to])->orderBy('id', 'desc');
      
      }


         $orders = $orders->paginate(10)->appends(['OrderAgent'=>$request['OrderAgent'],'user_id'=>$request['user_id'],'mobile'=>$request['mobile'],'datefrom'=>$request->datefrom,'dateto'=>$request->dateto]);

         $balance = $orders->sum('sale_price_in_dollar');
      
        

         return view('orders',['orders' => $orders, 'users'=>$users,'pusers'=>$pusers,'statuses' => $statuses,'customers'=>$customers,'brands'=>$brands,'agents'=>$agents,'totals' =>$balance]);
        
    }

    // order Details
    public function order_details($id){
        
        $orders = Order::with('customer','saleBy','purchasedBy','status')->find($id);
        return view('order-details',['orders' => $orders]);
    }
    public function getMyCustomrs(){

        $customers = Customer::with('orders')->where('user_id',backpack_user()->id)->paginate(10);
        return view('mycustomers',['customers' => $customers]);
    }
}
