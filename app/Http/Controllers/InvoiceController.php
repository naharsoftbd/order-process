<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Order;
use App\Invoice;
use App\Agent;
use App\User;
class InvoiceController extends Controller
{
	public function __construct()
    {
        //$this->middleware('isAdmin');
        $this->middleware(backpack_middleware());
    }
    public function index(){

        return redirect('orders');
    }
    //Making invoice 
    public function createInvoice($ids,Request $request){
            
            $string = "$ids"; 
            $ids_arr = explode (",", $string);  
            $subTotal = 0; 
            $bkashCharge = 0;
            $deliveryCharge = 0;
            $advancePaid =0;
            $customerid = 0;
            $agentid = 0;
           foreach($ids_arr as $id){
            $order = Order::find($id);
            $subTotal += floatval($order->total_order_amount);
            $bkashCharge += floatval($order->bkash_charge);
            $deliveryCharge += floatval($order->for_courier_charge);
            $advancePaid += floatval($order->bkash_receive + $order->cash_receive);
            $customerid = $order->customer_id;
            $agentid = $order->agent_id;
            }

            $invoice = new Invoice();
            $invoice->subTotal = floatval($subTotal);
            $invoice->bkashCharge = floatval($bkashCharge);
            $invoice->deliveryCharge = floatval($deliveryCharge);
            $invoice->advancePaid = floatval($advancePaid);
            $invoice->discount = floatval($request->input('discount'));
            $invoice->agent_id = $agentid;
            $invoice->user_id = backpack_user()->id;
            $invoice->customer_id = $customerid;
            $invoice->Save();
            $invoice->orders()->sync($ids_arr);
            return $agentid;
    }

    public function getInvoices(){

    	$invoices = Invoice::with('orders')->where('user_id',backpack_user()->id)->orderBy('id', 'desc')->get();
    	return view('invoice.index',['invoices'=>$invoices]);
    }

    public function downloadInvoice($id){

    	$data['invoice'] = Invoice::with('agent')->find($id);
    	$pdf = \PDF::loadView('pdf.invoice', $data);
		return $pdf->download('invoice.pdf');

    }

    public function invoiceDetails($id){

        $data['invoice'] = Invoice::with('agent')->find($id);
        
        return view('invoice.details',$data);

    }
}
