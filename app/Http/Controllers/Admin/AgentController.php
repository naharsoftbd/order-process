<?php

namespace App\Http\Controllers\Admin;

//use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\AgentRequest as StoreRequest;
use App\Http\Requests\AgentRequest as UpdateRequest;

class AgentController extends CrudController
{
    public function setup() {
        $this->crud->setModel('App\Agent');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/agent');
        $this->crud->setEntityNameStrings('agent', 'agents');
        $this->crud->setColumns(['name','email','mobileNo','agentAddress']);
        $this->crud->addFields([[
	'name' => 'name',
	'label' => 'Agent name'
	],[
                'label' => 'Users',
                'type' => 'select2_multiple',
                'name' => 'agents',
                'entity' => 'agents',
                'attribute' => 'name',
                'model' => "App\User",
                'pivot' => true,
                
            ]]);
        $this->crud->addField([
	'name' => 'email',
	'label' => 'Email'
	]);
        $this->crud->addField([
	'name' => 'mobileNo',
	'label' => 'Mobile Number'
	]);
    $this->crud->addField([
	'name' => 'agentAddress',
	'label' => 'Address',
	'type' => 'address',
    
	]);

	$this->crud->addField([ // base64_image
    'label' => "Agento Logo",
    'name' => "agentLogo",
    //'filename' => "agentLogo", // set to null if not needed
    'type' => 'image',
    'aspect_ratio' => 0, // set to 0 to allow any aspect ratio
    'crop' => true, // set to true to allow cropping, false to disable
    'upload' => true, // null to read straight from DB, otherwise set to model accessor function
]);
	
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
