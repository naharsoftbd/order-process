<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Order;
use Carbon\Carbon;
use App\Agent;
use App\User;

class ReportController extends CrudController
{
	public function __construct()
    {
        //$this->middleware('isAdmin');
        $this->middleware(backpack_middleware());
    }

    public function index(){
        $agents = Agent::all();
        $users = User::all();
        $orders = Order::with('customer','saleBy','status','agent')->paginate(10);
        return view('reports.index',['orders'=>$orders,'agents'=>$agents,'users'=>$users]);
    }
    public function dailySummary(Request $request)
    {
        $agents = Agent::all();
        $advanceadjusted = $request->input('advanceadjusted');
        $csvfrom = $request->input('report_from');
        $csvto = $request->input('report_to');
        $shippingDate = $request->input('csvshipping');
        $site = $request->input('site');
        $saleby = $request->input('saleby');

       $fromDate = new Carbon($csvfrom); 
       $toDate = new Carbon($csvto);
        $shippingDate = new Carbon($shippingDate);
        $orders = Order::with('customer','saleBy','status','agent')->where('advance_adjusted',$advanceadjusted)->where('agent_id',$site)->where('sale_by',$saleby)->orWhere('shipping_at',$shippingDate)->whereBetween('created_at', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()))->orderBy('id', 'desc')->paginate(10);

        
    	return view('reports.daily_summary',['orders'=>$orders,'agents'=>$agents]);
        //return $orders;
    }
}
