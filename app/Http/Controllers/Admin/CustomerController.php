<?php

namespace App\Http\Controllers\Admin;

//use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\CustomerRequest as StoreRequest;
use App\Http\Requests\CustomerRequest as UpdateRequest;

class CustomerController extends CrudController
{
    public function setup() {
        $this->crud->setModel('App\Customer');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/customer');
        $this->crud->setEntityNameStrings('customer', 'customers');
        $this->crud->setColumns(['customer_name','email','mobile','address','zipcode','state']);
        $this->crud->addField([
	'name' => 'customer_name',
	'label' => 'Customer name'
	]);
        $this->crud->addField([
	'name' => 'email',
	'label' => 'Email'
	]);
        $this->crud->addField([
	'name' => 'mobile',
	'label' => 'Mobile Number'
	]);
    $this->crud->addField([
	'name' => 'address',
	'label' => 'Address',
	'type' => 'address',
    
	]);
	$this->crud->addField([
	'name' => 'zipcode',
	'label' => 'zipcode'
	]);
	$this->crud->addField([
	'name' => 'state',
	'label' => 'State'
	]);
	$this->crud->addField([
	'name' => 'user_id',
	'entity' => 'user',
    'attribute' => 'name',
	'label' => 'User',
	'type' => 'select2',
	'model' => "App\User"
	]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
