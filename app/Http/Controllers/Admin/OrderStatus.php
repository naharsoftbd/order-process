<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\StatusRequest as StoreRequest;
use App\Http\Requests\StatusRequest as UpdateRequest;

class OrderStatus extends CrudController
{
    public function setup() {
        $this->crud->setModel('App\Status');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/status');
        $this->crud->setEntityNameStrings('status', 'order_statuses');

        $this->crud->setColumns(['status']);
        $this->crud->addField([
	'name' => 'status',
	'label' => 'Status name'
	]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
