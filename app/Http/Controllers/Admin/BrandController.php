<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\BrandRequest as StoreRequest;
use App\Http\Requests\BrandRequest as UpdateRequest;

class BrandController extends CrudController
{
    public function setup() {
        $this->crud->setModel('App\Brand');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/brand');
        $this->crud->setEntityNameStrings('brand', 'Brands');

        $this->crud->setColumns(['name']);
        $this->crud->addField([
	'name' => 'name',
	'label' => 'Brand name'
	]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
