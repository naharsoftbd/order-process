<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\User;
use App\Order;
class Customer extends Model
{
    use CrudTrait;

    protected $table = 'customers';
    protected $fillable = ['customer_id','customer_name','email','mobile','address','zipcode','state','user_id'];
    const CUSTOMER_NAME = 'Customer Name';
    //const UPDATED_AT = 'last_update';


    public function user(){
    	return $this->belongsTo(User::class);
    }
    public function orders(){

    	return $this->hasMany(Order::class);

    }
}
