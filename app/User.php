<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Backpack\Base\app\Models\BackpackUser;
use DB;
use App\Agent;
use App\Customer;
//use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable 
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $guard_name = 'web';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
        {
            return $this->getKey();
        }
        public function getJWTCustomClaims()
        {
            return [];
        }

        public static function getRoleBy($role)
        {
            $role = Role::where('name', $role)->get()->first();
            $users = DB::table('users')
            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->select('users.*')
            ->where('model_has_roles.role_id', '=', $role->id)
            ->get();
            return $users;
        }

        public function agent(){

            return $this->belongsToMany(Agent::class,'agent_user','user_id','agent_id');
        }

        public function customer(){
                return $this->hasOne(Customer::class);
            }
        
}
