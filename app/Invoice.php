<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Agent;
use App\User;
use App\Customer;


class Invoice extends Model
{
    public function getProduct($id){
    	return Order::find($id)->product_name;
    }

    public function getProducts($id){
    	return Order::whereId($id)->first();
    }

    public function agent(){

    		return $this->belongsTo(Agent::class);
    }
    public function user(){
    		return $this->belongsTo(User::class);
    }

    public function customer(){
                return $this->belongsTo(Customer::class);
            }
    public function orders(){
        return $this->belongsToMany(Order::class);
    }
}
