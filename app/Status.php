<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Status extends Model
{
    use CrudTrait;

    protected $table = 'order_statuses';
    protected $fillable = ['status'];
}
