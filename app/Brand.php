<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Order;

class Brand extends Model
{
    use CrudTrait;

    protected $table = 'brands';
    protected $fillable = ['name'];


    public function orders(){
    	return $this->belongsToMany(Order::class);
    }
}
